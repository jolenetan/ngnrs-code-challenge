async function getPrices()
        {
            //await the response of the fetch call
           let response = await fetch('https://static.ngnrs.io/test/prices');
            //proceed once the first promise is resolved.
           let data = await response.json()
            //proceed only when the second promise is resolved
            return data;
        }

getPrices()
    .then(data => {
      console.log(data);
      const test = data.data;

      for(var i = 0; i < test.prices.length; i++) {
        const mid = (test.prices[i].buy + test.prices[i].sell)/2;
        const pair = test.prices[i].pair;
        const quote = pair.slice(-3);
        console.log('Mid price for ' +  test.prices[i].pair  + ' is ' + mid + ' ' + quote + '.');
      }


        // test.forEach(price => {
        //     console.log(`Mid price for ${ price.pair } .`);
        // });
    }).catch(error => {
        console.log(error);
    });
