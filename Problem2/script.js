function validate() {
  var address = document.getElementById('input-address').value;
  var otp = document.getElementById('input-otp').value;

  var addressRgx = /^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/;
  var addressVal = addressRgx.test(address);

  var otpRgx = /^[0-9]{6}$/;
  var otpVal = otpRgx.test(otp);

  if (addressVal == true || otpVal == true) {
    alert('Success!');
  } else {
    alert("Please try again!");
  }

}
